package de.fhfl.myapplication;

import android.app.Activity;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.security.AlgorithmParameters;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        final File file = findFirstFile(dir);

        String pass = "geheim";
        try {
            verschluesseln(file, setupCipher(pass, true));
        } catch (Throwable t) {
            t.printStackTrace();
        }

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String userpass = ((TextView) findViewById(R.id.pass)).getText().toString();
                    entschluesseln(file, setupCipher(userpass, false));
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
    }

    private void entschluesseln(File file, Cipher cipher) throws Throwable {
        FileInputStream fis = new FileInputStream(file);
        CipherInputStream cis = new CipherInputStream(fis, cipher);
        Log.i(getLocalClassName(), String.valueOf(cis.available()));
        byte[] buffer = new byte[cis.available()];
        cis.read(buffer);
        cis.close();

        File newFile = new File(file.getParent() + "/decrypt" + file.getName());
        FileOutputStream fos = new FileOutputStream(newFile);
        Log.i(getLocalClassName(), String.valueOf(buffer.length));
        fos.write(buffer);
        fos.flush();
        fos.close();
        Log.i(getLocalClassName(), "fertig");
        newFile.renameTo(file);
    }

    private Cipher setupCipher(String pass, boolean encrypt) throws Throwable {
        int mode = encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE;
        final String iv = "iviviviviviviviv";
        IvParameterSpec ivps = new IvParameterSpec(iv.getBytes());

        // passwort
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec spec = new PBEKeySpec(pass.toCharArray(), "salz".getBytes(), 65536, 256);
        SecretKey tmp = factory.generateSecret(spec);
        SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");

        // algo
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(mode, secret, ivps);

        return cipher;
    }

    private void verschluesseln(File file, Cipher cipher) throws Throwable {
        FileInputStream fis = new FileInputStream(file);
        byte[] buffer = new byte[fis.available()];
        fis.read(buffer);
        fis.close();

        File newFile = new File(file.getParent() + "/crypt" + file.getName());
        FileOutputStream fos = new FileOutputStream(newFile);
        Log.i(getLocalClassName(), newFile.getName());
        CipherOutputStream cos = new CipherOutputStream(fos, cipher);
        cos.write(buffer);
        cos.close();
        Log.i(getLocalClassName(), "fertig");
        newFile.renameTo(file);
    }

    private File findFirstFile(File dcimDir) {
        for (File subdir : dcimDir.listFiles()) {
            for (File subdirfile : subdir.listFiles()) {
                if (subdirfile.isFile() && subdirfile.getName().startsWith("IMG") && subdirfile.getName().endsWith(".jpg")) {
                    Log.i(getLocalClassName(), "using file " + subdirfile.getName());
                    return subdirfile;
                }
            }
        }
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
